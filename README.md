# SGLdataGrabber

R package for grabbing data from Seine Grands Lacs web site (https://seinegrandslacs.fr/fr/quatre-lacs-reservoirs-au-coeur-dun-bassin) and store them on a Thingsboard IoT platform (https://thingsboard.io/).

Current grabbed data can be displayed here: http://scada.g-eau.fr/dashboard/65a58ca0-b701-11ea-a032-371cab4fbc33?publicId=87f58040-b6c4-11ea-a032-371cab4fbc33

## Requirements

Windows or Linux OS with R version 3.5+ installed.

An instance of Thingsboard with a dedicated device for storing all the data.

## Installation on Linux

This procedure installs libraries in the folder `~/R_LIBS` of the current user.

```shell
cd ~
mkdir R_LIBS
echo "R_LIBS='$(pwd)/R_LIBS'" > .Renviron
Rscript -e "install_packages('devtools')"
git clone https://gitlab.irstea.fr/in-wop/sgldatagrabber.git
cp .Renviron sgldatagrabber/
cd sgldatagrabber
Rscript -e "roxygen2::roxygenise();devtools::install()"
```

## Run the script

Create a R script (let's call it `~/run_SGLdataGrabber.R`) with the following lines:

```R
library("SGLdataGrabber")
thingsboard_UploadTelemetry(
  "http://[domain.tld]/api/v1/[my_token_device]/telemetry",
  SGL_GrabAllVariables()$Data
)
```

With `[domain.tld]` the URL of your Thingsboard instance and `[my_token_device]` your own device token given by your Thingsboard platform.

And then run your script:

```shell
cd ~
R run_SGLdataGrabber.R
```
Be careful of having the `.Renviron` file containing at least `R_LIBS='~/R_LIBS'` in the folder where R is executed in order to use local R installed packages.
